//
//  BeaconData.swift
//  HeatMapDemo
//
//  Created by Kevin Deem on 5/3/17.
//  Copyright © 2017 Kevin Deem. All rights reserved.
//

//import Foundation
import CoreData

class BeaconData {
  
  let massMurder = false // kill off old data
  
  
  var context:NSManagedObjectContext!
  var guests:[String:GuestInfo] = [:]
  let stations:[Int:Listener]
  var timer:Timer?
  var updateCallback:((Listener) -> Void)?
  var callCount = 0
  
  static let shared:BeaconData = {
    let instance = BeaconData()
    return instance
  }()
  
  init() {
    stations = [
      1:Listener(name:"Cocktail Lounge", id:1, x:198.0, y:234.0),
      2:Listener(name:"Tour Booking", id:2, x:437.0, y:579.0)
    ]
  }
  
  
  
  func startUpdates() {
    beaconFetch()
    guard timer == nil else {
      return
    }
    timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(beaconFetch), userInfo: nil, repeats: true)
    print("starting update timer")
  }
  
  
  
  func stopUpdates() {
    timer?.invalidate()
    timer = nil
    print("stopping update timer")
  }
  
  
  
  @objc func beaconFetch() {
    for (_,listener) in stations {
      getBeaconsFor(listener: listener)
    }
    
    //getAllBeaconData()
  }
  
  
  
  func findGuestFor(beacon:BeaconInfo) {
    let key = "\(beacon.major)/\(beacon.minor)"
    if let guest = guests[key] {
      //print("found \(guest.value(forKey: "guestName")!) for (\(key))")
      beacon.guest = guest
    } else {
      //print("no guest for (\(key)) ??? \(guests.count)")
    }
  }
  
  /*
  func getAllBeaconData() {
    if let url = URL(string: "https://9qiw4xcm88.execute-api.us-east-1.amazonaws.com/dev/getbeacontraffic") {
      let req = URLRequest(url: url, cachePolicy:.reloadIgnoringLocalCacheData, timeoutInterval: 5.0)
      let session = URLSession.shared
      let task = session.dataTask(with: req) {(responseData, response, error) in
        guard let data:Data = responseData, let _:URLResponse = response, error == nil else {
          print("Query failed (\(error.debugDescription))")
          print("response:\n\(String(describing: response?.debugDescription))")
          print("data:\n\(String(describing: responseData?.debugDescription))")
          return
        }
        do {
          let json = try JSONSerialization.jsonObject(with: data)
          if let problem = json as? [String:Any], let message = problem["message"] {
            print("message is \"\(message)\"")
            if response != nil {
              print(response!)
            }
          } else if let data = json as? [[String:Any]] {
            var beacons:[Int:[BeaconInfo]] = [:]
            for (id,_) in self.stations {
              beacons[id] = []
            }
            //print(json)
            for item in data {
              
              
              if let listenerIdString = item["listenerId"],
                let listenerId = listenerIdString as? Int,
                let listener = self.stations[listenerId],
                beacons[listenerId] != nil {
                let info = BeaconInfo(data: item)
                self.findGuestFor(beacon: info)
                beacons[listenerId] = beacons[listenerId]! + [info]
                print("count \(listenerId):\(listener.beacons.count)")
              } else {
                print("Invalid: json:\n\(item)")
              }
            }
            for (id,beacon) in beacons {
              self.stations[id]?.beacons = beacon
            }
          } else {
            print("Unknown: \(json)")
          }
          self.callCount += 1
          self.updateCallback?(self)
        } catch {
          print("Threw while reading data")
        }
        
      }
      task.resume()
    }
  }
 */
  
  
  
  func getBeaconsFor(listener:Listener) {
    let id = listener.id
    
    if let url = URL(string: "https://9qiw4xcm88.execute-api.us-east-1.amazonaws.com/dev/getbeacontraffic/\(id)") {
      let req = URLRequest(url: url, cachePolicy:.reloadIgnoringLocalCacheData, timeoutInterval: 4.0)
      let session = URLSession.shared
      let task = session.dataTask(with: req) {(responseData, response, error) in
        guard let data:Data = responseData, let _:URLResponse = response, error == nil else {
          print("Query failed (\(error.debugDescription))")
          print("response:\n\(String(describing: response?.debugDescription))")
          print("data:\n\(String(describing: responseData?.debugDescription))")
          return
        }
        do {
          var beacons:[BeaconInfo] = []
          let json = try JSONSerialization.jsonObject(with: data)
          
          if let problem = json as? [String:Any], let message = problem["message"] {
            print("message is \"\(message)\"")
            if response != nil {
              
              print(response!)
            }
          } else if let dataWrapper = (json as? [[String:Any]])?.first,
            let data = dataWrapper["beacons"] as? [[String:Any]] {
            
            //print("json for \(id): \(data)")
            for item in data {
              //print(item.debugDescription)
              let info = BeaconInfo(data: item)
              self.findGuestFor(beacon: info)
              if info.guest?.vip == "Yes" {
                  beacons.insert(info, at: 0)
              } else {
                beacons.append(info)
              }
            }
            print("id \(id) has \(data.count) and \(beacons.count)")
          } else if let dataWrapper = json as? [String:Any],
            let data = dataWrapper["beacons"] as? [[String:Any]] {
            
            //print("json for \(id): \(data)")
            for item in data {
              //print(item.debugDescription)
              let info = BeaconInfo(data: item)
              self.findGuestFor(beacon: info)
              if info.guest?.vip == "Yes" {
                beacons.insert(info, at: 0)
              } else {
                beacons.append(info)
              }
            }
            print("no array wrapper added: id \(id) has \(data.count) and \(beacons.count)")
          } else {
            print("Nope(\(id)) \(json))\n\(response.debugDescription)")
          }
          self.callCount += 1
          listener.beacons = beacons
          self.updateCallback?(listener)
          
        } catch {
          print("Threw while reading data")
        }
        
      }
      task.resume()
    }

  }
  
  
  /*
   
   Andreas – change name to Andrea
   Swap images  Zhou to Lepell, Lepell to Zhoy   (or swap names)
   Swap images  Tommy Cheung for Manis Jerry, Jr
   Fix names Manis Jerry, Jr. to Jerry Manis
   Rena Criss – change city from Rugby to Youngstown
   Shin Iwasaki – change name to Sherri Iwasaki
   Ronald Wong – change hame to Ronald Williams
 */
  
  
  func populatePeople(_ context:NSManagedObjectContext) -> [GuestInfo] {
    let peeps:[String:[String:String]] = [
      "Shangzhen Zhou":["vip":"No", "major":"10", "minor":"10", "profilePic":"man2", "age":"25", "home":"New York, USA"],
      "Paul David Lepell":["vip":"No", "major":"10", "minor":"20", "profilePic":"man1", "age":"28", "home":"San Deigo, USA"],
      "Everett Donovan":["vip":"No", "major":"10", "minor":"30", "profilePic":"man3", "age":"22", "home":"Tampa Bay, USA"],
      "Deborah Nofte":["vip":"No", "major":"10", "minor":"40", "profilePic":"girl1", "age":"28", "home":"Boise, USA"],
      "Gary Huey":["vip":"No", "major":"10", "minor":"50", "profilePic":"man4", "age":"38", "home":"Tallahassee, USA"],
      "Lloyd Arisumi":["vip":"No", "major":"10", "minor":"60", "profilePic":"man5", "age":"23", "home":"Brooklyn, USA"],
      "Andrea Christoforidis":["vip":"Yes", "major":"10", "minor":"70", "profilePic":"andreas", "age":"23", "home":"Toronto, Canada"],
      "Alan Doell":["vip":"Yes", "major":"10", "minor":"80", "profilePic":"man7", "age":"26", "home":"Chicago, USA"],
      "Chris Fox":["vip":"Yes", "major":"10", "minor":"90", "profilePic":"chris", "age":"45", "home":"Miami, USA"],
      "Joyce Paul":["vip":"No", "major":"10", "minor":"100", "profilePic":"girl2", "age":"21", "home":"Los Angeles, USA"],
      "Barbara Taforo":["vip":"No", "major":"10", "minor":"110", "profilePic":"girl3", "age":"38", "home":"Fargo, USA"],
      "Rosalina Neeley":["vip":"No", "major":"10", "minor":"120", "profilePic":"girl4", "age":"23", "home":"Alberta, Canada"],
      "Fay Wong":["vip":"No", "major":"10", "minor":"130", "profilePic":"girl5", "age":"28", "home":"Phoenix, USA"],
      "Vincent Hanson":["vip":"No", "major":"10", "minor":"140", "profilePic":"man9", "age":"32", "home":"Bronx, USA"],
      "Barbara Acosta":["vip":"No", "major":"10", "minor":"150", "profilePic":"girl6", "age":"35", "home":"Toledo, USA"],
      "Clifford Reiss":["vip":"No", "major":"10", "minor":"160", "profilePic":"man10", "age":"35", "home":"New Orleans, USA"],
      "Patricia Martin":["vip":"Yes", "major":"10", "minor":"170", "profilePic":"girl7", "age":"19", "home":"Portland, USA"],
      "Tommy Cheung":["vip":"No", "major":"10", "minor":"180", "profilePic":"man6", "age":"32", "home":"Austin, USA"],
      "Marian Butcher":["vip":"No", "major":"10", "minor":"190", "profilePic":"girl8", "age":"22", "home":"Charleston, USA"],
      "Victor Jin":["vip":"No", "major":"10", "minor":"200", "profilePic":"man12", "age":"25", "home":"Norfolk, USA"],
      "Garth Spatz":["vip":"Yes", "major":"10", "minor":"210", "profilePic":"man13", "age":"33", "home":"Springfield, USA"],
      "Fortuna Rosalinda":["vip":"No", "major":"10", "minor":"220", "profilePic":"girl9", "age":"34", "home":"Reno, USA"],
      "Rena Criss":["vip":"No", "major":"10", "minor":"230", "profilePic":"girl10", "age":"46", "home":"Youngstown, USA"],
      "Nerissa Mendoza":["vip":"No", "major":"10", "minor":"240", "profilePic":"girl11", "age":"38", "home":"Seattle, USA"],
      "Richard Domholt":["vip":"No", "major":"10", "minor":"250", "profilePic":"man14", "age":"28", "home":"Albany, USA"],
      "Jerry Manis":["vip":"No", "major":"10", "minor":"260", "profilePic":"man11", "age":"24", "home":"Colorado Springs, USA"],
      "Ronald Williams":["vip":"No", "major":"10", "minor":"270", "profilePic":"man15", "age":"22", "home":"Sioux Falls"],
      "Sherri Iwasaki":["vip":"Yes", "major":"10", "minor":"280", "profilePic":"girl14", "age":"25", "home":"Barymore, USA"],
      "Sharon Barclay":["vip":"No", "major":"10", "minor":"290", "profilePic":"girl13", "age":"26", "home":"Alberta, Canada"], // not live
      "Ulla Pajala":["vip":"No", "major":"10", "minor":"300", "profilePic":"girl15", "age":"32", "home":"Brooklyn, USA"], // not live
      "Adrian Hodgkinson":["vip":"No", "major":"10", "minor":"310", "profilePic":"girl15", "age":"31", "home":"Springfield, USA"], // not live
      "Rebecca Stees":["vip":"No", "major":"10", "minor":"320", "profilePic":"girl17", "age":"30", "home":"Sescatuan, USA"], // not live  man6
      "John Swafford":["vip":"No", "major":"10", "minor":"1", "profilePic":"man16", "age":"29", "home":"Melk, USA"],
      "John Shafer":["vip":"No", "major":"10", "minor":"4", "profilePic":"man17", "age":"28", "home":"Olaf, USA"],
      "Robert Borromeo":["vip":"No", "major":"10", "minor":"3", "profilePic":"girl18", "age":"27", "home":"Holla, USA"],
      "Karen Haines":["vip":"No", "major":"10", "minor":"2", "profilePic":"girl19", "age":"26", "home":"Magoya, USA"]
    ]
    
    var rv:[GuestInfo] = []
    
    if let desc = NSEntityDescription.entity(forEntityName: "GuestInfo", in: context) {
      for (peep,details) in peeps {
        
        let (arrivalDate, departureDate) = getTravelDates()
        
        let guest = NSManagedObject(entity: desc, insertInto: context) as! GuestInfo
        guest.guestName = peep
        guest.vip = details["vip"]
        guest.age = details["age"]
        guest.home = details["home"]
        guest.major = Int16(details["major"]!)!
        guest.minor = Int16(details["minor"]!)!
        guest.arrivalDate = Date(timeIntervalSince1970: arrivalDate.timeIntervalSince1970) as Date
        guest.departDate = NSDate(timeIntervalSince1970: departureDate.timeIntervalSince1970) as Date
        guest.profilePic = details["profilePic"]
        
        let key = "\(details["major"]!)/\(details["minor"]!)"
        
        print("create: \(peep) = (\(key))")
        
        rv.append(guest)
      }
    }
    return rv
  }
  
  
  
  func getTravelDates() -> (NSDate,NSDate) {
    let stayLength = arc4random_uniform(4)
    var stayLengthInterval = TimeInterval(0)
    var arrivalInterval = TimeInterval(0)
    let secondsInDay = 86400
    
    switch stayLength {
    case 1:
      stayLengthInterval = TimeInterval(secondsInDay * 5)
      arrivalInterval = TimeInterval(secondsInDay * Int(arc4random_uniform(2) + 1))
    case 2:
      stayLengthInterval = TimeInterval(secondsInDay * 7)
      arrivalInterval = TimeInterval(secondsInDay * Int(arc4random_uniform(3) + 1))
    case 3:
      stayLengthInterval = TimeInterval(secondsInDay * 14)
      arrivalInterval = TimeInterval(secondsInDay * Int(arc4random_uniform(5) + 1))
    default:
      stayLengthInterval = TimeInterval(secondsInDay * 3)
      arrivalInterval = TimeInterval(secondsInDay)
    }
    let arrivalDate = NSDate(timeIntervalSinceNow: -arrivalInterval)
    let departureDate = arrivalDate.addingTimeInterval(stayLengthInterval)
    return (arrivalDate, departureDate)
  }
  
  
  
  func massMurderBeforeDataReload() {
    
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "GuestInfo")
    let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
    do {
      try context.execute(deleteRequest)
      try context.save()
    } catch {
    }
  }
  
  
  
  func updateTravelDates() {
    
    if massMurder {
      massMurderBeforeDataReload()
    }
    
    let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "GuestInfo")
    do {
      var people = try context.fetch(fetchRequest) as! [GuestInfo]
      if people.count < 1 {
        people = populatePeople(context)
        prepare(guestData: people)
      } else {
        prepare(guestData: people)
      }
      for guest in people {
        let (arrivalDate, departureDate) = getTravelDates()
        guest.setValue(arrivalDate, forKey: "arrivalDate")
        guest.setValue(departureDate, forKey: "departDate")
        fillInOtherStuff(on:guest)
      }
      try context.save()
      
    }
    catch let error as NSError {
      print("Epic fail \(error), \(error.userInfo)")
    }
  }
  
  
  
  func fillInOtherStuff(on:GuestInfo) {
    on.topCategory = topCategories[Int(arc4random_uniform(4))]
    on.topPurchase = topPurchases[Int(arc4random_uniform(4))]
    on.topAmenity = topAmenities[Int(arc4random_uniform(4))]
    
    let cal = Calendar.current
    let arrived = cal.startOfDay(for: on.arrivalDate! as Date)
    let now = cal.startOfDay(for: Date())
    let components = cal.dateComponents([.day], from: arrived, to: now)
    let daysHere = components.day
    
    var tours:[String:Float] = [:]
    var totalDrinkCost:Float = 0.0
    var totalDrinks = 0
    var drinksPerDay:[Int] = []
    var drinkCostPerDay:[Float] = []
    var drinkTally:[String:Int] = [
      "Corona":0,
      "Premium Mojito":0,
      "Moscow Mule":0,
      "Veuve Clicquat Rose":0
    ]
    if let daysHere = daysHere, daysHere > 0 {
      for _ in 0 ..< daysHere {
        if let name = findNewTour(tours) {
          tours[name] = costs[name]
        }
        var drinksToday = 0
        var drinkCostToday:Float = 0.0
        
        
        for _ in 0 ..< Int(arc4random_uniform(5) + 1) {
          let drinkName = clVenue[Int(arc4random_uniform(4))]
          let tally = drinkTally[drinkName]! + 1
          drinkTally[drinkName] = tally
          totalDrinks += 1
          drinksToday += 1
          let drinkCost = costs[drinkName]!
          totalDrinkCost += drinkCost
          drinkCostToday += drinkCost
        }
        
        
        drinksPerDay.append(drinksToday)
        drinkCostPerDay.append(drinkCostToday)
      }
      
      // find 3 highest drinkTypes
      var least = 100
      var leastName = ""
      
      for (name, tally) in drinkTally {
        if least > tally {
          least = tally
          leastName = name
        }
      }
      
      // build bought list
      var clBought = ""
      for (name,tally) in drinkTally {
        if name != leastName && tally > 0 {
          if clBought != "" {
            clBought += ", "
          }
          clBought += "\(name)(\(tally))"
        }
      }
      
      var tbBought = ""
      var totalTourCost:Float = 0.0
      for (name,cost) in tours {
        if tbBought != "" {
          tbBought += ", "
        }
        tbBought += "\(name)(\(1))"
        totalTourCost += cost
      }
    
      on.clVisits = Int16(drinksPerDay.count)
      on.clAvTime = Int16(totalDrinks * 20 / drinksPerDay.count)
      on.clBoughtList = clBought
      on.clVisitSpend = totalDrinkCost / Float(drinksPerDay.count)
      
      on.tbVisits = Int16(tours.count)
      if tours.count > 0 {
        on.tbAvTime = Int16((90 + arc4random_uniform(320)))
        on.tbVisitSpend = totalTourCost / Float(tours.count)
      } else {
        on.tbAvTime = 0
        on.tbVisitSpend = 0
      }
      on.tbBoughtList = tbBought
      
      on.dailySpend = on.tbVisitSpend + on.clVisitSpend
      on.averagePurchase = (totalTourCost + totalDrinkCost) / Float(totalDrinks + tours.count)
      on.totalRevenue = totalTourCost + totalDrinkCost + (Float(arc4random_uniform(15000)) / 100.0)
      
      on.credits = Float(arc4random_uniform(800000)) / 100.0 + 4000.0
      
    } else {
      
      on.clVisits = 0
      on.clAvTime = 0
      on.clBoughtList = ""
      on.clVisitSpend = 0.0
      
      on.tbVisits = 0
      on.tbAvTime = 0
      on.tbBoughtList = ""
      on.tbVisitSpend = 0.0
      
      on.dailySpend = 0.0
      on.averagePurchase = 0.0
      on.totalRevenue = 0.0
      
      on.credits = Float(arc4random_uniform(8000))  + 4000.0
    }
    
    
  }
  
  
  func findNewTour(_ tours:[String:Float]) -> String? {
    if tours.count > 3 {
      return nil
    }
    var name = ""
    repeat {
      name = tbVenue[Int(arc4random_uniform(4))]
    } while tours[name] != nil
    return name
  }
  
  func prepare(guestData:[GuestInfo]) {
    var g:[String:GuestInfo] = [:]
    for guest in guestData {
      let key = "\(guest.major)/\(guest.minor)"
      g[key] = guest
    }
    guests = g
  }
  
  
  
  func fakeBeaconData() {
    for (_,listener) in stations {
      listener.fakeBeaconData()
    }
  }
  
  // MARK: - fudge data support objects
  
  let topCategories = [
    "Spa & Wellness",
    "Restaurants",
    "Tours",
    "Sports"
  ]
  
  let topPurchases = [
    "Veuve Clicquat Rose",
    "Scuba Excursion",
    "Executive Spa Treatment",
    "Exclusive Dinner"
  ]
  
  let topAmenities = [
    "Giavanni's Restaurant",
    "Cocktail Lounge",
    "Tour Booking",
    "Poolside Buffet"
  ]
  
  let clVenue = [
    "Corona",
    "Premium Mojito",
    "Moscow Mule",
    "Veuve Clicquat Rose"
  ]
  
  let tbVenue = [
    "Waveboard",
    "Turtle Island Dive",
    "Windsurfing",
    "Local Excursion"
  ]
  
  let costs:[String:Float] = [
    "Corona":4.99,
    "Premium Mojito":9.99,
    "Moscow Mule":8.99,
    "Veuve Clicquat Rose":9.99,
    "Scuba Excursion":439.99,
    "Turtle Island Dive":679.99,
    "Windsurfing Rental":149.99,
    "Excellent Exploration Excursion":249.99
  ]
  
  let venueFields  = [
    "cl":[
      "clVisits",
      "clAvTime",
      "clVisitSpend"
    ],
    "tb":[
      "tbVisits",
      "tbAvTime",
      "tbVisitSpend"
    ]
  ]
  
}


class Listener {
  
  var name:String
  var id:Int
  var x:Double
  var y:Double
  var beacons:[BeaconInfo]
  
  init(name:String, id:Int, x:Double, y:Double) {
    self.name = name
    self.id = id
    self.x = x
    self.y = y
    beacons = []
  }
  
  func fakeBeaconData() {
    let uuid = "e3520a1d-9c1a-435b-90ef-6e2bc1307496"
    var beaconData:[BeaconInfo] = []
    for index in 1...10 {
      let rssi = -Int(arc4random_uniform(100))
      let accuracy = Float(rssi) / Float(arc4random_uniform(100) + 50)
      beaconData.append(BeaconInfo(uuid: uuid, major: 10, minor: (index * 10), rssi: rssi, accuracy: accuracy, timestamp: Int64(Date().timeIntervalSince1970)))
    }
  }
  
  func nameText() -> String {
    return "\(id). \(name)"
  }
  
  func attendanceText() -> String {
    let guestLabel = "guest\(beacons.count != 1 ? "s" : "")"
    return "\(beacons.count) \(guestLabel) | \(getVipCountText()) VIP"
  }
  
  func getVipCountText() -> String {
    var count = 0
    for beacon in beacons {
      if beacon.guest != nil && beacon.guest!.vip == "Yes" {
        count += 1
      }
    }
    return "\(count)"
  }
  
  func getCapacityLabel() -> String {
    return "\(Int(Double(beacons.count) / 25.0 * 100.0))%"
  }
}



class BeaconInfo {
  var uuid:String
  var major:Int
  var minor:Int
  var rssi:Int
  var accuracy:Float
  var timestamp:Int64
  
  var guest:GuestInfo?
  
  init(uuid:String, major:Int, minor:Int, rssi:Int, accuracy:Float, timestamp:Int64) {
    self.uuid = uuid
    self.major = major
    self.minor = minor
    
    self.rssi = rssi
    self.accuracy = accuracy
    self.timestamp = timestamp
  }
  
  init(data:[String:Any]) {
    uuid = ((data["UUID"] != nil) ? data["UUID"] as! String : "")
    major = ((data["major"] != nil) ? data["major"] as! Int : 0)
    minor = ((data["minor"] != nil) ? data["minor"] as! Int : 0)
    rssi = ((data["rssi"] != nil) ? (data["rssi"] as! NSNumber).intValue : 0)
    accuracy = ((data["accuracy"] != nil) ? (data["accuracy"] as! NSNumber).floatValue : 0.0)
    timestamp = ((data["timestamp"] != nil) ? data["timestamp"] as! Int64 : 0)
  }
}


