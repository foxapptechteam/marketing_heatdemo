//
//  ViewController.swift
//  HeatMapDemo
//
//  Created by Kevin Deem on 5/1/17.
//  Copyright © 2017 Kevin Deem. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var map: UIImageView!
  @IBOutlet weak var venueTable: UITableView!
  @IBOutlet weak var liveFlasher: UILabel!
  
  var cocktailLounge:ListenerPinVC!
  var tourBooking:ListenerPinVC!
  
  
  let beaconData = BeaconData.shared
  var currentListener: Listener?
  var currentGuest:GuestInfo?
  let background = UIImage(named: "mapBackground")
  
  
  static let greenColor = UIColor.green
  static let yellowColor = UIColor.yellow
  static let redColor = UIColor.red
  
  static let tealColor = UIColor(red: 80/255.0, green: 227.0/255.0, blue: 194.0/255.0, alpha: 1.0)  // 80 227 194
  static let squashColor = UIColor(red: 245.0/255.0, green: 227.0/255.0, blue: 194.0/255.0, alpha: 1.0)
  static let blueColor = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0 / 255.0, alpha: 1.0)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    //if let size = background?.size {
      //map.bounds = CGRect(x: 0, y: 0, width: size.width, height: size.height)
      //scrollView.contentSize = size
      //print("size is \(size.debugDescription)")
    //}
    //view.contentScaleFactor = 3.0
    print("size is \(view.frame.size.debugDescription), \(view.contentScaleFactor)")
    
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    beaconData.updateCallback = {(listener) in
      DispatchQueue.main.async {
        self.liveFlasher.isHidden = !(self.liveFlasher.isHidden)
        self.venueTable.reloadData()
        if listener.id == 1 {
          self.cocktailLounge.updateData()
        } else {
          self.tourBooking.updateData()
        }
      }
    }
    BeaconData.shared.startUpdates()
    cocktailLounge.profileSelected = {(guest, listener) in
      self.currentGuest = guest
      self.currentListener = listener
      self.performSegue(withIdentifier: "ShowProfileList", sender: self)
      print("going to cocktail lounge with \(String(describing: guest.guestName))")
    }
    tourBooking.profileSelected = {(guest, listener) in
      self.currentGuest = guest
      self.currentListener = listener
      self.performSegue(withIdentifier: "ShowProfileList", sender: self)
      print("going to tour booking with \(String(describing: guest.guestName))")
    }
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    BeaconData.shared.stopUpdates()
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowProfileList" {
      let profileList = segue.destination as! GuestProfileList
      profileList.set(guestData:currentGuest, listener:currentListener)
    } else if segue.identifier == "CocktailLounge" {
      cocktailLounge = segue.destination as! ListenerPinVC
      cocktailLounge.listenerId = 1
    } else if segue.identifier == "TourBooking" {
      tourBooking = segue.destination as! ListenerPinVC
      tourBooking.listenerId = 2
    }
  }
  
  @IBAction func unwindFromProfileList(segue:UIStoryboardSegue) {
    
  }
  
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return beaconData.stations.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "VenueCell", for: indexPath) as! VenueListCell
    cell.listener = indexPath.row == 0 ? beaconData.stations[1] : beaconData.stations[2]
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print("Row \(indexPath.row)")
    
    currentListener = indexPath.row == 0 ? beaconData.stations[1] : beaconData.stations[2]
    performSegue(withIdentifier: "ShowProfileList", sender: self)
    
    tableView.deselectRow(at: indexPath, animated: true)
  }
}


class VenueListCell: UITableViewCell {
  
  @IBOutlet weak var venueLabel: UILabel!
  @IBOutlet weak var guestCountLabel: UILabel!
  @IBOutlet weak var capacityLabel: UILabel!
  var listener:Listener? {
    didSet (listener) {
      if let l = listener {
        venueLabel.text = "\(l.id). \(l.name)"
        var tag = "guest"
        if l.beacons.count != 1 {
          tag += "s"
        }
        guestCountLabel.text = "\(l.beacons.count) \(tag) | \(l.getVipCountText()) VIP"
        capacityLabel.text = "\(l.getCapacityLabel()) capacity"
      } else {
        venueLabel.text = ""
        guestCountLabel.text = ""
        capacityLabel.text = ""
      }
    }
  }
  
}

