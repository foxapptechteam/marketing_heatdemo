//
//  File.swift
//  HeatMapDemo
//
//  Created by Kevin Deem on 5/10/17.
//  Copyright © 2017 Kevin Deem. All rights reserved.
//

import UIKit
import CoreData

class GuestProfileList : UIViewController {
  
  
  @IBOutlet weak var profileList: UICollectionView!
  
  weak var profileDetails:GuestProfileDetails!
  @IBOutlet weak var guestCountLabel: UILabel!
  @IBOutlet weak var vipCountLabel: UILabel!
  @IBOutlet weak var occupancyLabel: UILabel!
  
  @IBOutlet weak var scroller: UIScrollView!
  
  var guest:GuestInfo?
  var listener:Listener?
  var guestOrder:[GuestInfo] = []
  
  //let lightGreenColor = UIColor(displayP3Red: 126.0/255.0, green: 126.0/255.0, blue: 33.0/255.0, alpha: 1.0)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    //print("size is \(view.frame.size.debugDescription), \(view.contentScaleFactor)")
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    //scroller.contentSize = CGSize(width: 588, height: 1278 + profileList.frame.height)
    scroller.translatesAutoresizingMaskIntoConstraints = true
  }
  
  
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    profileDetails.set(guestData: guest, listener: listener)
    BeaconData.shared.updateCallback = {(listener) in
      if listener.id == self.listener?.id {
        self.listener = listener
        DispatchQueue.main.async {
          self.set(guestData: self.guest, listener: self.listener)
        }
      }
    }
    BeaconData.shared.startUpdates()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    BeaconData.shared.stopUpdates()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ProfileDetails" {
      profileDetails = segue.destination as! GuestProfileDetails
    }
  }
  
  func set(guestData:GuestInfo?, listener:Listener?) {
    self.guest = guestData
    self.listener = listener
    if guest == nil && listener != nil && (listener?.beacons.count)! > 0  {
      self.guest = listener!.beacons.first!.guest
    }
    profileDetails?.set(guestData: self.guest, listener: listener)
    if let l = listener {
      guestCountLabel?.text = "\(l.beacons.count)"
      vipCountLabel?.text = l.getVipCountText()
      occupancyLabel?.text = l.getCapacityLabel()
    } else {
      guestCountLabel?.text = "0"
      vipCountLabel?.text = "0"
      occupancyLabel?.text = "0%"
    }
    profileList?.reloadData()
  }
  
  
}

extension GuestProfileList : UICollectionViewDataSource, UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    guard let list = listener else {
      print("No listener in profile list")
      return 0
    }
    //print(" pic collection size \(collectionView.frame.size)")
    var gtemp:[GuestInfo] = []
    for beacon in list.beacons {
      if let guest = beacon.guest {
        if guest.vip == "Yes" {
          gtemp.insert(guest, at: 0)
        } else {
          gtemp.append(guest)
        }
      }
    }
    guestOrder = gtemp
    let rows = 1 + guestOrder.count / 9
    let height = CGFloat(rows * 48 + 52)
    for constraint in collectionView.constraints {
      if constraint.firstAttribute == NSLayoutAttribute.height {
        constraint.constant = height
      }
    }
    //scroller.contentSize = CGSize(width: 588, height: 1278 + height)
    
    collectionView.setNeedsLayout()
    scroller.setNeedsLayout()
    //collectionView.autoresizingMask =UIViewAutoresizing.
    
    return guestOrder.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileListPic", for: indexPath) as! ProfilePicCell
    cell.setup(guestOrder[indexPath.row], selected:guestOrder[indexPath.row] == guest)
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.row < guestOrder.count {
      set(guestData: guestOrder[indexPath.row], listener: listener)
    }
  }
}

class ProfilePicCell : UICollectionViewCell {
  @IBOutlet weak var profilePic: UIImageView!
  var guest:GuestInfo?
  
  func setup(_ guest:GuestInfo?, selected:Bool) {
    self.guest = guest
    let bounds = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
    let renderer = UIGraphicsImageRenderer(bounds: bounds)
    let image = renderer.image { (cx) in
      if let imgName = guest?.profilePic, let img = UIImage(named:imgName) {
        img.draw(in: bounds, blendMode:CGBlendMode.colorDodge, alpha: (selected ? 1.0 : 0.75))
        if guest?.vip == "Yes" {
          cx.cgContext.setStrokeColor(ViewController.tealColor.cgColor)
          cx.cgContext.setLineWidth(2.0)
          cx.cgContext.strokeEllipse(in: CGRect(x: 1.0, y: 1.0, width: 38.0, height: 38.0))
        }
      }
      
    }
    profilePic.image = image
    profilePic.layer.masksToBounds = true
    profilePic.layer.cornerRadius = 20
    
  }
  
}

class GuestProfileDetails : UIViewController {
  
  
  private var guest:GuestInfo?
  private var listener:Listener?
  
  @IBOutlet weak var guestNameLabel: UILabel!
  @IBOutlet weak var guestPic: UIImageView!
  @IBOutlet weak var vipMemberTag: UILabel!
  @IBOutlet weak var checkInLabel: UILabel!
  @IBOutlet weak var checkOutLabel: UILabel!
  @IBOutlet weak var ageLabel: UILabel!
  @IBOutlet weak var homeLabel: UILabel!
  
  @IBOutlet weak var venueLabel: UILabel!
  @IBOutlet weak var visitsLabel: UILabel!
  @IBOutlet weak var avTimeLabel: UILabel!
  @IBOutlet weak var revVisitLabel: UILabel!
  @IBOutlet weak var mostPurchasedLabel: VerticalTopAlignLabel!
  
  @IBOutlet weak var totalRevenueLabel: UILabel!
  @IBOutlet weak var avPurchaseLabel: UILabel!
  @IBOutlet weak var dailySpendLabel: UILabel!
  
  @IBOutlet weak var topCategoryLabel: UILabel!
  @IBOutlet weak var topPurchaseLabel: UILabel!
  @IBOutlet weak var topAmenityLabel: UILabel!
  @IBOutlet weak var totalCreditsLabel: UILabel!
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
  }
  override func viewDidLayoutSubviews() {
    guestPic.layer.cornerRadius = guestPic.layer.bounds.width / 2.0
    guestPic.layer.masksToBounds = true
  }
  
  func set(guestData:GuestInfo?, listener:Listener?) {
    self.guest = guestData
    self.listener = listener
    if let guest = self.guest {
      guestNameLabel.text = guest.guestName
      if guest.vip == "Yes" {
        vipMemberTag.textColor = .black
      } else {
        vipMemberTag.textColor = .white
      }
      guestPic.image = UIImage(named: guest.profilePic!)
      
      let formatter = DateFormatter()
      formatter.dateStyle = .medium
      checkInLabel.text = formatter.string(from: guest.arrivalDate! as Date)
      checkOutLabel.text = formatter.string(from: guest.departDate! as Date)
      
      ageLabel.text = guest.age
      homeLabel.text = guest.home
      
      venueLabel.text = genVenueLabel(listener)
      
      let currency = NumberFormatter()
      currency.numberStyle = .currency
      currency.locale = Locale(identifier: "en_US")
      
      if listener?.id == 1 {
        visitsLabel.text = "\(guest.clVisits)"
        avTimeLabel.text = "\(guest.clAvTime)"
        revVisitLabel.text = currency.string(from: NSNumber(value: guest.clVisitSpend))
        mostPurchasedLabel.text = guest.clBoughtList
      } else  {
        visitsLabel.text = "\(guest.tbVisits)"
        avTimeLabel.text = "\(guest.tbAvTime)"
        revVisitLabel.text = currency.string(from: NSNumber(value: guest.tbVisitSpend))
        mostPurchasedLabel.text = guest.tbBoughtList
      }
      topCategoryLabel.text = guest.topCategory
      topPurchaseLabel.text = guest.topPurchase
      topAmenityLabel.text = guest.topAmenity
      totalRevenueLabel.text = currency.string(from: NSNumber(value: guest.totalRevenue))
      totalCreditsLabel.text = "\(currency.string(from: NSNumber(value: guest.credits))!) Resort Credits"
      avPurchaseLabel.text = currency.string(from: NSNumber(value: guest.averagePurchase))
      dailySpendLabel.text = currency.string(from: NSNumber(value: guest.dailySpend))
      
    } else {
      
      print("No guest in details")
    }
    
  }
  
  func genVenueLabel(_ listener:Listener?) -> String {
    guard let l = listener else {
      return "When visiting somewhere"
    }
    if l.id == 1 {
      return "When Visiting the Cocktail Lounge"
    }
    return "When Visiting Travel Booking"
  }
}

class VerticalTopAlignLabel: UILabel {
  
  override func drawText(in rect:CGRect) {
    guard let labelText = text else {  return super.drawText(in: rect) }
    
    let attributedText = NSAttributedString(string: labelText, attributes: [NSAttributedStringKey.font: font])
    var newRect = rect
    newRect.size.height = attributedText.boundingRect(with: rect.size, options: .usesLineFragmentOrigin, context: nil).size.height
    
    if numberOfLines != 0 {
      newRect.size.height = min(newRect.size.height, CGFloat(numberOfLines) * font.lineHeight)
    }
    
    super.drawText(in: newRect)
  }
  
}
