//
//  ListenerPinVc.swift
//  HeatMapDemo
//
//  Created by Kevin Deem on 5/23/17.
//  Copyright © 2017 Kevin Deem. All rights reserved.
//

import UIKit
import CoreData


class ListenerPinVC: UIViewController {
  
  let beaconData = BeaconData.shared
  var beacons:[BeaconInfo] = []
  var listenerId:Int = 1
  
  @IBOutlet weak var pinView: PinView!
  @IBOutlet weak var venueLabel: UILabel!
  @IBOutlet weak var guestsLabel: UILabel!
  @IBOutlet weak var calloutWrapper: UIView!
  @IBOutlet weak var collectionView: UICollectionView!
  
  
  var profileSelected:((GuestInfo, Listener) -> Void)?
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    calloutWrapper.layer.cornerRadius = 8.0
    calloutWrapper.layer.shadowColor = UIColor.black.cgColor
    calloutWrapper.layer.shadowOpacity = 1.0
    calloutWrapper.layer.shadowOffset = CGSize.zero
    calloutWrapper.layer.shadowRadius = 10.0
    calloutWrapper.layer.needsDisplayOnBoundsChange = true
    setVisiblity()
    
  }
  
  func setVisiblity() {
    let showLabel = venueLabel.text != nil && venueLabel.text != ""
    let showProfiles = beacons.count > 0
    pinView.calloutVisible = showLabel
    calloutWrapper.isHidden = !showLabel
    collectionView.isHidden = !showProfiles
  }
  
  func updateData() {
    //print("view Height: \(collectionView?.frame.size.height)")
    if let listener = beaconData.stations[listenerId] {
      venueLabel.text = listener.nameText()
      guestsLabel.text = listener.attendanceText()
      
      var tempBeacons:[BeaconInfo] = []
      for beacon in listener.beacons {
        if let guest = beacon.guest {
          if guest.vip == "Yes" {
            tempBeacons.insert(beacon, at: 0)
          } else {
            tempBeacons.append(beacon)
          }
          if tempBeacons.count >= 19 {
            break
          }
        }
      }
      beacons = tempBeacons
      
      
      setVisiblity()
      collectionView.reloadData()
      pinView.calloutVisible = true
      calloutWrapper.setNeedsLayout()
      collectionView.setNeedsLayout()
      pinView.setNeedsDisplay()
      collectionView.setNeedsDisplay()
      
      calloutWrapper.layer.cornerRadius = 8.0
      
      //let maskPath = UIBezierPath(roundedRect: calloutWrapper.bounds,
      //                            byRoundingCorners: [.topLeft, .topRight, .bottomLeft, .bottomRight],
      //                            cornerRadii: CGSize(width: 8.0, height: 8.0))
      //let maskLayer = CAShapeLayer()
      //maskLayer.path = maskPath.cgPath
      //calloutWrapper.layer.mask = maskLayer
      
      calloutWrapper.layer.shadowColor = UIColor.black.cgColor
      calloutWrapper.layer.shadowOpacity = 1.0
      calloutWrapper.layer.shadowOffset = CGSize.zero
      calloutWrapper.layer.shadowRadius = 10.0
      calloutWrapper.layer.needsDisplayOnBoundsChange = true
      calloutWrapper.layer.masksToBounds = false
      
      
    }
  }
  
  
}

extension ListenerPinVC : UICollectionViewDataSource, UICollectionViewDelegate {
  
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
    var h:CGFloat = 0.0
    if beacons.count > 0 {
      h += 38.0
    }
    if beacons.count > 5 {
      h += 35.0
    }
    if beacons.count > 10 {
      h += 35.0
    }
    if beacons.count > 15 {
      h += 35.0
    }
    
    calloutWrapper.frame.size.height = h + 54.0
    //collectionView.frame.size.height = h
    //collectionView.frame.size.width = calloutWrapper.frame.width - 8.0
    //print("new view Height: \(collectionView?.frame.size.height)")
    
    if beacons.count ==  19 { // add ellipse
      return 20
    }
    return beacons.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    if indexPath.row < beacons.count {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfilePicView
      
      cell.set(beacon:beacons[indexPath.row])
      return cell
    } else {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreIcon", for: indexPath) as! MoreIconCell
      
      return cell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.row < beacons.count, let guest = beacons[indexPath.row].guest, let name = guest.guestName {
      print("Selected \(name)")
      self.profileSelected?(guest, beaconData.stations[listenerId]!)
    } else {
      print("Selected row \(indexPath.row)")
    }
  }
}

class PinView: UIView {
  
  var calloutVisible = false
  
  override func draw(_ rect: CGRect) {
    if let cx = UIGraphicsGetCurrentContext() {
      
      // draw halo
      let midpoint = bounds.width / 2.0
      var size = midpoint - 10.0
      var offset = 10.0
      /*
       greenColor.set()
       cx.setAlpha(0.5)
       let rect = CGRect(x: offsetX, y: offsetY, width: rad*2.0, height: rad*2.0)
       cx.fillEllipse(in: rect)
       */
      
      // draw grey outer circle (20px)
      cx.setAlpha(1.0)
      UIColor.lightGray.setFill()
      
      cx.fillEllipse(in: CGRect(x: midpoint - 12.0, y: midpoint - 12.0, width: 24.0, height: 24.0))
      
      // draw white cicle (16px)
      UIColor.white.setFill()
      size -= 4.0
      offset += 2.0
      cx.fillEllipse(in: CGRect(x: midpoint - 10.0, y: midpoint - 10.0, width: 20.0, height: 20.0))
      
      UIColor.red.set()
      size -= 4.0
      offset += 2.0
      cx.fillEllipse(in: CGRect(x: midpoint - 8.0, y: midpoint - 8.0, width: 16.0, height: 16.0))
      
      if calloutVisible {
        UIColor.white.setFill()
        UIColor.white.setStroke()
        cx.move(to: CGPoint(x: bounds.width/2.0, y: bounds.width/2.0))
        cx.addLine(to: CGPoint(x: bounds.width, y: midpoint - 8.0))
        cx.addLine(to: CGPoint(x: bounds.width, y: midpoint + 8.0))
        cx.addLine(to: CGPoint(x: bounds.width/2.0, y: bounds.width/2.0))
        cx.fillPath()
      }
    }
  }
}



class ProfilePicView: UICollectionViewCell {
  var beacon:BeaconInfo?
  @IBOutlet weak var imageView: UIImageView!
  //public var profileIndex:Int?
  
  
  func set(beacon:BeaconInfo?) {
    layer.cornerRadius = 15
    self.beacon = beacon
    if let beacon = beacon {
      let drawArea = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
      let render = UIGraphicsImageRenderer(bounds:drawArea)
      
      //let name = beacon.guest?.value(forKey: "guestName") as? String
      
      let image = render.image { (cx) in
        if let guest = beacon.guest, let imgName = guest.profilePic, let img = UIImage(named:imgName) {
          img.draw(in: drawArea)
          if guest.vip == "Yes" {
            cx.cgContext.setStrokeColor(ViewController.tealColor.cgColor)
            cx.cgContext.setLineWidth(2.0)
            cx.cgContext.strokeEllipse(in: CGRect(x: 1.0, y: 1.0, width: 28.0, height: 28.0))
          }
        }
        
      }
      
      imageView.image = image
    }
  }
  
  
  /*
   func set(guest:NSManagedObject?) {
   isHidden = guest == nil
   layer.cornerRadius = 15
   if let picName = guest?.value(forKey: "profilePic") as? String {
   image = UIImage(named: picName)
   //print("profile pic set to \(picName)")
   } else {
   //print("no guest pic (\(guest != nil))")
   }
   self.guest = guest
   setNeedsDisplay()
   }
   
   
   override func draw(_ rect: CGRect) {
   if let cx = UIGraphicsGetCurrentContext() {
   UIColor.black.set()
   cx.addRect(CGRect(x: 5.0, y: 5.9, width: 20.0, height: 20.0))
   var msg = "tag(\(tag)) | "
   if let name = guest?.value(forKey: "guestName") as? String {
   msg += "name:\(name)"
   if let img = image {
   img.draw(in: CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0))
   //cx.draw(img, in: CGRect(x: 0.0, y: 0.0, width: bounds.width, height: bounds.height))
   if guest?.value(forKey: "vip") as? String == "Yes" {
   msg += "(VIP)"
   cx.setLineWidth(2.0)
   ViewController.tealColor.set()
   cx.strokeEllipse(in: CGRect(origin: CGPoint(x:0.0, y: 0.0), size: CGSize(width: bounds.width - 1.0, height: bounds.height - 1.0)))
   } else  {
   msg += "()"
   }
   } else {
   msg += " -- No Image --"
   }
   print(msg)
   }
   } else {
   print("no context in \(tag)")
   }
   }
   */
  
}

class MoreIconCell : UICollectionViewCell {
  
}
